% readme.txt for stvrauth.cls
% Version 2.0 released 13 May 2010
%
% This software may only be used to prepare an article for publication in
% Software Testing, Verification and Reliability
% to be published by John Wiley & Sons, Ltd.
% Any other use constitutes an infringement of copyright.
%
% The release consists of the following files:
%
%   readme.txt   this file
%   stvrauth.cls  the LaTeX2e class file
%   stvrdoc.tex   authors' instructions
%   stvrdoc.pdf   authors' instructions in PDF format
%
% Typeset stvrdoc.tex for instructions and examples, or view the PDF.
%
% Simply place stvrauth.cls and stvrdoc.tex in your systems usual directories
% and typeset using your LaTeX2e command.
%
%
% Please note that the file wileyj.bst is available from the same download
% page for those authors using BibTeX.
%
%
% *** IMPORTANT NOTE ***
% When you submit your paper, please use the "doublespace" option
% in the documentclass line which will double-space your document
% and make the task of reviewing much simpler.
%
