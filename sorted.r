library(plotrix)
data = read.csv("C:/Users/seokhyeon/Desktop/매직 폴더/My papers/icst2014-muse-extension/stvrauth/sorted.csv")

pdf("C:/Users/seokhyeon/Desktop/매직 폴더/My papers/icst2014-muse-extension/stvrauth/sorted.pdf", width=6, height=4)

plot(data$op2~data$order, ylim=c(0,1), type="l", lty="solid", xlab="Executed Statements", ylab="Normalized Suspiciousness", main=expression("Distribution of suspiciousness for space v21"), cex.main=0.8)
lines(data$jaccard~data$order, ylim=c(0,1), type="l", lty="dashed")
lines(data$ochiai~data$order, ylim=c(0,1), type="l", lty="dotdash")
lines(data$hybrid_muse~data$order, ylim=c(0,1), type="l", lty="dotted")
legend("topright", legend=c(expression("Op2"), expression("Ochiai"), expression("Jaccard"), expression("Hybrid-MUSE")), lty=c("solid", "dotdash", "dashed", "dotted"))

#muse faulty statement
draw.circle(1,1, radius=20)
text(310, 1, expression("Hybrid-MUSE"), cex=0.7)

#op2 faulty statement
draw.circle(11,0.98, radius=20)
text(165, 0.95, expression("Op2"), cex=0.7)

#ochiai faulty statement
draw.circle(11,0.5, radius=20)
text(165, 0.5, expression("Ochiai"), cex=0.7)

#jaccard faulty statement
draw.circle(11,0.25, radius=20)
text(165, 0.25, expression("Jaccard"), cex=0.7)

dev.off()