import math

def normalise(d):
	return map(lambda s: s / sum(d), d)

def eps_replace(d, epsilon):
	return [epsilon if x == 0 else x for x in d]

def true_dist(length, location):
	d = [0] * length
	d[location] = 1
	return d

def lil(length, location, scores, epsilon):
	assert length == len(scores) 
	assert min(scores) >= 0 #negative scores are not allowed

	td = normalise(eps_replace(true_dist(length, location), epsilon))
	sd = normalise(eps_replace(normalise(scores), epsilon))
	
	tmp = [math.log(p / q) * p for (p, q) in zip(td, sd)]

	return sum(tmp)

if __name__ == "__main__":
	_q = [0.1, 0.2, 0.3, 0, 0]
	print lil(5, 2, _q, 0.05)