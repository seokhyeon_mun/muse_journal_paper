data = read.csv("C:/Users/seokhyeon/Desktop/매직 폴더/My papers/icst2014-muse-extension/stvrauth/distribution.csv")



pdf("C:/Users/seokhyeon/Desktop/매직 폴더/My papers/icst2014-muse-extension/stvrauth/barplot_muse.pdf", width=4, height=3)
barplot(data$hybrid_muse, ylim=c(0,1), main=expression("Hybrid-MUSE (LIL=3.37)"), xlab="Executed Statements", ylab="", cex.main=0.8, cex.lab=0.8, cex.axis=0.8, yaxs="r", mgp=c(0,1,0), omd=c(0.1, 1,1,1))
abline(v=3152, lty="dotted", col="white") 
text("Faulty Statement", x=3150, y=0.95, cex=0.8)
mtext("Suspiciousness", side=2, line=1.7, cex=0.8)
dev.off()

pdf("C:/Users/seokhyeon/Desktop/매직 폴더/My papers/icst2014-muse-extension/stvrauth/barplot_op2.pdf", width=4, height=3)
barplot(data$op2, ylim=c(0,1), main=expression("Op2 (LIL=7.34)"), xlab="Executed Statements", ylab="", cex.main=0.8, cex.lab=0.8, cex.axis=0.8, yaxs="r", mgp=c(0,1,0))
abline(v=3152, lty="dotted", col="white")
text("Faulty Statement", x=3150, y=0.2, cex=0.8, col="white")
mtext("Suspiciousness", side=2, line=1.7, cex=0.8)
dev.off()

pdf("C:/Users/seokhyeon/Desktop/매직 폴더/My papers/icst2014-muse-extension/stvrauth/barplot_jaccard.pdf", width=4, height=3)
barplot(data$jaccard, ylim=c(0,1), main=expression("Jaccard (LIL=4.92)"), xlab="Executed Statements", ylab="", cex.main=0.8, cex.lab=0.8, cex.axis=0.8, yaxs="r", mgp=c(0,1,0))
abline(v=3152, lty="dotted")
text("Faulty Statement", x=3150, y=0.95, cex=0.8)
mtext("Suspiciousness", side=2, line=1.7, cex=0.8)
dev.off()

pdf("C:/Users/seokhyeon/Desktop/매직 폴더/My papers/icst2014-muse-extension/stvrauth/barplot_ochiai.pdf", width=4, height=3)
barplot(data$ochiai, ylim=c(0,1), main=expression("Ochiai (LIL=5.96)"), xlab="Executed Statements", ylab="", cex.main=0.8, cex.lab=0.8, cex.axis=0.8, yaxs="r", mgp=c(0,1,0))
abline(v=3152, lty="dotted")
text("Faulty Statement", x=3150, y=0.95, cex=0.8)
mtext("Suspiciousness", side=2, line=1.7, cex=0.8)
dev.off()